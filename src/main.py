from model import StatusType, DoModel


if '__main__' == __name__:
    do_data: DoModel = DoModel()
    print(f"{do_data.updated_at} {do_data.status.value}")
