from enum import Enum
from dataclasses import dataclass
import datetime


class StatusType(Enum):
    NON_INITIALIZED = 'NON_INITIALIZED'
    ON = 'ON'
    OFF = 'OFF'


class Util:
    @staticmethod
    def get_minimal_datetime() -> datetime:
        return datetime.datetime(year=datetime.MINYEAR,
                                 month=1,
                                 day=1,
                                 hour=0,
                                 minute=0,
                                 second=0,
                                 microsecond=0)


@dataclass
class DoModel:
    """Class that contains basic information about an digital output record."""
    status: StatusType
    updated_at: datetime

    def __init__(self):
        self.update(status=StatusType.NON_INITIALIZED,
                    specific_datetime=Util.get_minimal_datetime())

    def update(self, status: StatusType,
               specific_datetime: datetime = datetime.datetime.now):
        self.status = status
        self.updated_at = specific_datetime
