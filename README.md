# 技術面試：Python

> v2024/04/09-1

---

## 0. 面試流程

1. 詢問面試意願：人力銀行平台
2. 一面：線上技術面試，使用 GitLab 提交答案，不限時間
3. 二面：電話或實體面試。通過一面後，由 "用人主管、同事、人資" 進行口試
4. 面試結果通知、意願確認

## 1. 面試說明

### a. 開發環境

Python 3.5+, CLI (Command-Line Interface) App 開發。

### b. 題目與時間

請將 "考題" -- [python-cli-app](https://gitlab.com/WiseOT-Interview-Exams/python-cli-app) 專案，從 GitLab clone 到你的工作環境，並使用你順手的開發工具來作答。

在職缺關閉前，沒有作答與繳交時間的限制。因此依您的步調進行即可。

### c. 專案 layout 

* 主程式 -- ``main.py``
* 資料類別 -- ``model.py``
    * ``DoModel`` 資料類別：用於紀錄工業控制中常見的 "控制點" ON / OFF 狀態
    * ON / OFF 狀態定義：``status`` 欄位，使用列舉型別 ``StatusType``
    * ON / OFF 狀態更新時間：``updated_at`` 欄位，使用 ``datetime.datetime`` 型別

|DoMode Class Diagram l|
|:------|
|Fields<br>+status: StatusType<br>+updated_at: datetime.datetime|
|Methods<br>-update(status: StatusType, specific_datetime: datetime.datetime)|


## 2. 實作題目
請依序實作以下需求，並以 git 提交對應的 commit logs，以及使用適當的 feature branch、或是 feature toggle 分支策略。

### a. 調整存取範圍層級

請依物件導向的基本觀念、目的，調整 ``DoModel`` fields、methods 的存取範圍層級 ([accessibility levels](https://learn.microsoft.com/zh-tw/dotnet/csharp/language-reference/keywords/accessibility-levels))。

### b. 撰寫單元測試

請幫 ``model.py`` 撰寫有效的單元測試；你可以 "補做" TDD，或是只撰寫單元測試。

### c. 模組化開發

1. 請將主程式中實例化 ``DoModel`` 的程式，改寫為以亂數隨機給予 ``StatusType`` 狀態
2. 請將上述隨機給予 ``StatusType`` 狀態的程式，封裝為一個 method、class，或是 util
3. 請為上述程式設計一個介面 (interface)，讓後續 ON / OFF 資料來源的 driver 開發，可以依賴於抽象而非實作

### d. 交付程式碼

請將上述完成的進度、你的 local repo，上推到本專案或你的 public repo，並以 [pull request](https://gitbook.tw/chapters/github/pull-request) 來繳交答案。

---

(以下空白)

